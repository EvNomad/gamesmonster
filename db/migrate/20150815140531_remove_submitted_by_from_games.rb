class RemoveSubmittedByFromGames < ActiveRecord::Migration
  def change
    remove_column :games, :submitted_by, :integer
  end
end
