class AddSubmitedByToGames < ActiveRecord::Migration
  def change
  	add_column :games, :submitted_by, :integer
  end
end
