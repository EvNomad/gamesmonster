class AddAmountToCopies < ActiveRecord::Migration
  def change
    add_column :copies, :amount, :integer
  end
end
