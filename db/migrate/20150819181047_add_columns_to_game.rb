class AddColumnsToGame < ActiveRecord::Migration
  def change
    add_column :games, :for_sale, :boolean
    add_column :games, :for_rent, :boolean
    add_column :games, :giveaway, :boolean
  end
end
