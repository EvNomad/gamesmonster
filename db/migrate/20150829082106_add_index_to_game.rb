class AddIndexToGame < ActiveRecord::Migration
  def change
    add_index :games, :title, unique: true
  end
end
