class AddDefaultValueToActiveGenres < ActiveRecord::Migration
  def change
    change_column :genres, :active, :boolean, :default => true
  end
end
