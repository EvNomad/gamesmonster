# == Schema Information
#
# Table name: games
#
#  id                 :integer          not null, primary key
#  title              :string
#  description        :text
#  genre_id           :integer
#  rating             :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  user_id            :integer
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  for_sale           :boolean
#  for_rent           :boolean
#  giveaway           :boolean
#

class Game < ActiveRecord::Base
  belongs_to :genre
  belongs_to :user

  has_many	:reviews
  has_many	:copies

  has_attached_file :image, :styles => { :medium => "470x360>", :thumb => "235x180>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates               :title, presence: true, length: 3..100
  validates_uniqueness_of :title
  validates_presence_of   :rating #, :genre,  :user
  
end
