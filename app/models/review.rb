# == Schema Information
#
# Table name: reviews
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  game_id    :integer
#  rating     :integer
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :game

  validates_presence_of :user, :game, :content
  
  validates :rating, length: { in: 0..10 }
  
  validates_uniqueness_of :game, :scope => :user
end
