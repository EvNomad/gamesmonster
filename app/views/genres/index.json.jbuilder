json.array!(@genres) do |genre|
  json.extract! genre, :id, :name, :active
  json.url genre_url(genre, format: :json)
end
