json.array!(@games) do |game|
  json.extract! game, :id, :title, :description, :genre_id, :rating
  json.url game_url(game, format: :json)
end
