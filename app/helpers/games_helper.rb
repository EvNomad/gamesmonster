module GamesHelper
    def avg_rating(game)
    	if game.reviews.any?
        	game.reviews.average(:rating).round(1)
        else
        	0
        end		
    end
end
