# == Schema Information
#
# Table name: games
#
#  id                 :integer          not null, primary key
#  title              :string
#  description        :text
#  genre_id           :integer
#  rating             :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  user_id            :integer
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  for_sale           :boolean
#  for_rent           :boolean
#  giveaway           :boolean
#

require 'rails_helper'

RSpec.describe Game, :type => :model do

  let(:title)   {'Cool Game'}
  let(:rating)  {'PG18'}

  # MVO
  subject(:game) {Game.new(title: title, rating: rating)}

  it 'should be valid' do
    expect(game).to be_valid
  end

  describe '#title' do

    context 'when not present' do
      let(:title) {''}
      it 'is not valid' do
        expect(subject).to be_invalid
      end
    end

    context 'when too short' do
      let(:title) {'12'}
      it 'is not valid' do
        expect(subject).to be_invalid
      end
    end

    context 'when too long' do
      let(:title) {'a'*101}
      it 'is not valid' do
        expect(subject).to be_invalid
      end
    end

    context 'when not unique' do
      let(:game_with_same_title) {subject.dup}
      before {game_with_same_title.save}
      it 'is not valid' do
        expect(subject).to be_invalid
      end
    end

  end



  # describe 'the game description' do
  #
  #   context 'when not exists' do
  #     subject.description = ''
  #     expect(subject).to be_invalid
  #   end
  #
  # end
  
end
